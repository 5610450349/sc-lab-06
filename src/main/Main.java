package main;

import java.util.ArrayList;

import model2.Animal;
import model2.Penguin;
import model.Aqua;
import model.Dolphin;
import model.Feeding;
import model.Nemo;
import model.Shark;
import model.Training;

public class Main {
	public static void main(String[] args){
		Animal ani = new Animal();
		Animal ani1 = new Animal("Kitty");
		Animal ani2 = new Animal("Kitty","Human");
		Dolphin dolphin = new Dolphin("Dolly");
		Nemo nemo = new Nemo("Momo");
		Shark shark = new Shark("Shock");
		
		ArrayList<Aqua> aquas = new ArrayList<Aqua>();
		System.out.println("----food----");
		aquas.add(dolphin);
		aquas.add(nemo);
		aquas.add(shark);
		
		for(Aqua aqua: aquas){
			System.out.println(aqua.eat());
		}
		System.out.println("-----Feeding-----");
		Feeding feed =  new Feeding();
		
		System.out.print(feed.feedAqua(dolphin));
		System.out.print(feed.feedAqua(nemo));
		System.out.print(feed.feedAqua(shark));
		

		System.out.print(feed.feedAqua((Aqua)dolphin));
		System.out.print(feed.feedAqua((Aqua)nemo));	
		System.out.print(feed.feedAqua((Aqua)shark));
		
		System.out.println("----Show-----");
		Training train = new Shark("Grey");
		train.playBall();
		train.hoop();
		train.jump();
		
	}

}