package model;

public class Nemo extends Aqua {
	public Nemo(String string){
		super(string);
	}

	@Override
	public String eat(){
		return "Daphnia";
	}
}
