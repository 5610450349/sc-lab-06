package model;

public class Feeding {
	public String feedAqua(Aqua aqua){
		return "Aqua "+aqua.eat()+"\n";
	}
	
	public String feedAqua(Nemo nemo){
		return "Nemo eat "+ nemo.eat()+"\n";
	}

	public String feedAqua(Dolphin dolphin){
		return "Dolphin eat " + dolphin.eat()+"\n";
	}
	
	public String feedAqua(Shark shark){
		return "Shark eat " + shark.eat()+"\n";
	}
}
