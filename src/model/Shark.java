package model;

public class Shark extends Aqua implements Training {
	public Shark(String string){
		super(string);
	}

	@Override
	public String eat(){
		return "fish";
	}
	
	public void hoop() {
		System.out.println("sift hoops!");
	}
	public void jump(){
		System.out.println("jump jump");
    }
	public void playBall(){
		System.out.println("ball boo");
    }
}
