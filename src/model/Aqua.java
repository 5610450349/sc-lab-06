package model;

public abstract class Aqua {
	private String name;
	
	public Aqua(String aName){
		this.name = aName;
	}

	public abstract String eat();
	
	public String toString() {
		return this.name;
	}
}
